/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazifeladat3;

/**
 *
 * @author milan
 */
public class HaziFeladat3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Lampa a = new Lampa();
        a.setNev("fejlampa");
        a.setForras("elem");
        a.setHordozhato(true);
        a.setLumen(1000);
        Lampa b = new Lampa();
        b.setNev("zseblampa");
        b.setForras("napelem");
        b.setHordozhato(true);
        b.setLumen(1200);
        Lampa c = new Lampa();
        c.setNev("csillár");
        c.setForras("halozat");
        c.setHordozhato(false);
        c.setLumen(2500);
        
        System.out.println(a.getNev() +": "+ a.getForras() +", "+ a.getHordozhato() +", "+ a.getLumen());
        System.out.println(b.getNev() +": "+ b.getForras() +", "+ b.getHordozhato() +", "+ b.getLumen());
        System.out.println(c.getNev() +": "+ c.getForras() +", "+ c.getHordozhato() +", "+ c.getLumen());
    }
    
}

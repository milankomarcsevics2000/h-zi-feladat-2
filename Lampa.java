/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazifeladat3;

/**
 *
 * @author milan
 */
public class Lampa {
    private String nev;
    private String forras;
    private Boolean hordozhato;
    private Integer lumen;
    
     public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }
    public void setForras(String forras) {
        this.forras = forras;
    }
    public String getForras() {
        return forras;
    }
    public void setHordozhato(Boolean hordozhato){
        this.hordozhato = hordozhato;
    }
    public Boolean getHordozhato(){
        return hordozhato;
    }
    public void setLumen(Integer lumen){
        this.lumen = lumen;
    }
    public Integer getLumen(){
        return lumen;
    }
   
}
